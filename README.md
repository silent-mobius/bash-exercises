# Bash Exercises

## _Instructions_

In order to use this repository, fork it to your account, create hidden folder name `.ans` and in it create a __seperate__ file for each task.
When done, push the code for merge and code review.

- [Variables](./Exercises_Bash.md#Variables)